/*
 * Okom - A platform of supervised cognitive exercises
 * Copyright (C) 2023 Oscar Alberto Quijano Xacur
 *
 * This file is part of Okom.
 *
 * Okom is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Okom is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Okom.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var results_section=document.querySelector("#results_table");
var presults_input=document.querySelector("#patient-results");

class doctorPatientLocale  extends dynamicLocale{
    constructor(){
	super("#db-locale");
    }

    locale_path(locale){
	return `/static/po/${locale}/doctor_patient.json`;
    }
}

var dpatloc=new doctorPatientLocale();


if(presults_input.value != ""){

    var presults=JSON.parse(presults_input.value);

    var showing_tablep=false;

    var exercises={};
    var exercises_array=presults.map((x)=>{
	let exercise_name=Object.keys(x)[0];
	exercises[exercise_name]  =  x[exercise_name];
	return exercise_name;
    });

    exercises_array.sort((a,b)=>{
	if(exercises[a][0].date < exercises[b][0].date )
	    return 1
	else
	    return -1});

    var select_wrapper=document.createElement("div");
    select_wrapper.classList.add("select");
    var exercise_select=document.createElement("select");
    select_wrapper.appendChild(exercise_select);

    results_section.appendChild(select_wrapper);

    function create_option(option_name){
	var option = document.createElement("option");
	option.value = option_name;
	option.text = option_name;
	return option;
    }

    for(let name of exercises_array){
	exercise_select.add(create_option(name));
    }



    function has_errorsp(exercise_name){
	return Object.keys(exercises[exercise_name][0]).includes("errors");
    }

    function exercise_Ncycles(exercise_name){
	return exercises[exercise_name][0]["time"].length ;
    }

    function create_header_text_element(content1,content2=null){
	var th=document.createElement("th");
	var text=document.createTextNode(content1);
	th.appendChild(text);
	dpatloc.add_display_message(text,content1);
	if( content2 != null ){
	    var text2 = text=document.createTextNode(` ${content2}`);
	    th.appendChild(text2);
	}
	return th;
    }


    function row_add_cell(row,content){
	var cell=row.insertCell();
	var content=document.createTextNode(content);
	cell.appendChild(content);
    }
    

    function add_table_header(table,exercise_name){
	var include_errors = has_errorsp(exercise_name);
	var Ncyles=exercise_Ncycles(exercise_name);
	var thead = table.createTHead();
	var header_row = thead.insertRow();

	header_row.appendChild(create_header_text_element(dpatloc.i18n.gettext("Date")));
	for(let i=1 ; i<=Ncyles ; i++){
	    header_row.appendChild(create_header_text_element(dpatloc.i18n.gettext("Time"),i));
	    if(include_errors){
		header_row.appendChild(create_header_text_element(dpatloc.i18n.gettext("Errors"),i));
	    }
	}
    }

    function add_table_contents(table,exercise_name){
	var include_errors = has_errorsp(exercise_name);
	var Ncyles=exercise_Ncycles(exercise_name);
	var info=exercises[exercise_name];
	for(let row_info of info){
	    let row=table.insertRow();	
	    row_add_cell(row,row_info.date.replace(/([^ ]+) ([^ ]+)/,'$1'));	
	    for(let i = 0; i< Ncyles;i++){
		row_add_cell(row,row_info.time[i]);
		if(include_errors){
		    row_add_cell(row,row_info.errors[i]);
		}
	    }	
	}
	
    }

    function create_results_table(exercise_name){

	var results_table=document.createElement("table");
	results_table.className="table";
	add_table_header(results_table,exercise_name);
	add_table_contents(results_table,exercise_name);
	return results_table;
    }

    function display_results_table(){
	var table_to_show=create_results_table(exercise_select.value);
	if(showing_tablep){
	    results_section.removeChild(results_section.lastChild);
	}
	
	results_section.appendChild(table_to_show);
	showing_tablep=true;
    }



    exercise_select.onchange=display_results_table;
    dpatloc.finished_loading.then(()=>{
	display_results_table();
    });

}else{
    var text=document.createElement("p");
    var no_results_message=document.querySelector("#no-results-message");
    
    text.innerHTML=no_results_message.value;
    results_section.appendChild(text);
}

class ChangableText{
    
    constructor(box_id,text_to_show,action_url=null){
	this.box=document.querySelector(`#${box_id}`);
	this.current_text=text_to_show;
	this.action_url=action_url;
	this.show_text();
    }

    show_text(){
	this.box.innerHTML=`<span class="icon-text">
	<div id="text">${this.current_text}</div>
	<span class="icon is-size-5">
	  <a id="edit-link">
	    <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fmonticons, Inc. --><path d="M441 58.9L453.1 71c9.4 9.4 9.4 24.6 0 33.9L424 134.1 377.9 88 407 58.9c9.4-9.4 24.6-9.4 33.9 0zM209.8 256.2L344 121.9 390.1 168 255.8 302.2c-2.9 2.9-6.5 5-10.4 6.1l-58.5 16.7 16.7-58.5c1.1-3.9 3.2-7.5 6.1-10.4zM373.1 25L175.8 222.2c-8.7 8.7-15 19.4-18.3 31.1l-28.6 100c-2.4 8.4-.1 17.4 6.1 23.6s15.2 8.5 23.6 6.1l100-28.6c11.8-3.4 22.5-9.7 31.1-18.3L487 138.9c28.1-28.1 28.1-73.7 0-101.8L474.9 25C446.8-3.1 401.2-3.1 373.1 25zM88 64C39.4 64 0 103.4 0 152V424c0 48.6 39.4 88 88 88H360c48.6 0 88-39.4 88-88V312c0-13.3-10.7-24-24-24s-24 10.7-24 24V424c0 22.1-17.9 40-40 40H88c-22.1 0-40-17.9-40-40V152c0-22.1 17.9-40 40-40H200c13.3 0 24-10.7 24-24s-10.7-24-24-24H88z"/></svg>
	  </a>	
	</span>
      </span>`;
	var edit_button=this.box.querySelector("#edit-link");
	var this_aux=this;
	edit_button.onclick= (e) => {
	    edit_button.onclick=null;
	    this_aux.show_edit();
	};	
    }

    show_edit(){

	if(! this.action_url){	    
	    this.box.innerHTML=`<div class="field has-addons">
	<div class="l">
	  <input class="input" type="text" id="new-name" value="${this.current_text}" >
	</div>
	<div class="">
	  <button class="button is-info" id="new-name-button">
            ${dpatloc.i18n.gettext("Change name")}
	  </button>
	</div>
      </div>`;
	    
	    var change_button=this.box.querySelector("#new-name-button");
	    var new_name=this.box.querySelector("#new-name");
	    new_name.focus();
	    new_name.setSelectionRange(new_name.value.length,new_name.value.length);
	    var this_aux=this;
	    change_button.onclick= (e) => {
		
		this_aux.show_text();
	    };
	    new_name.onkeydown=(e)=>{
		console.log(e.key);
		switch (e.key){
		case "Enter":
		    this_aux.current_text=new_name.value;
		    this_aux.show_text();
		    break;
		case "Escape":
		    this_aux.show_text();
		    break;
		}
	    }; 
	}else{
	    this.box.innerHTML=`<form action="${this.action_url}">
<div class="field has-addons">
	<div class="control">
	  <input class="input" type="text" id="new-name" value="${this.current_text}" name="new-name">
	</div>
	<div class="control">
	  <input class="button is-info" id="new-name-button" type="submit" value="${dpatloc.i18n.gettext("Change name")}">
	</div>
      </div>
</form>`;
	    var change_button=this.box.querySelector("#new-name-button");
	    var new_name=this.box.querySelector("#new-name");
	    new_name.focus();
	    new_name.setSelectionRange(new_name.value.length,new_name.value.length);
	    var this_aux=this;

	    new_name.oninput=(e)=>{
		if(new_name.value ==""){
		    change_button.disabled = true;
		}else{
		    change_button.disabled = false;
		}
	    }
	    new_name.onkeydown=(e)=>{
		// console.log(e.key);
		switch (e.key){
		case "Enter":
		    change_button.click();
		    break;
		case "Escape":
		    e.preventDefault();
		    this_aux.show_text();
		    break;
		}

		
	    }; 

	}

    }    
}

document.addEventListener('DOMContentLoaded', () => {

  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Add a click event on each of them
  $navbarBurgers.forEach( el => {
    el.addEventListener('click', () => {

      // Get the target from the "data-target" attribute
      const target = el.dataset.target;
      const $target = document.getElementById(target);

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      el.classList.toggle('is-active');
      $target.classList.toggle('is-active');

    });
  });

});

document.addEventListener('DOMContentLoaded', () => {
  (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
    const $notification = $delete.parentNode;

    $delete.addEventListener('click', () => {
      $notification.parentNode.removeChild($notification);
    });
  });
});
