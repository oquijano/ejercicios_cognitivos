/*
 * Okom - A platform of supervised cognitive exercises
 * Copyright (C) 2023 Oscar Alberto Quijano Xacur
 *
 * This file is part of Okom.
 *
 * Okom is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Okom is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Okom.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var META_NAME;

class controlLocale extends dynamicLocale{
    constructor(){
	super("#db-locale");
    }

    locale_path(locale){	
	return `/static/po/${locale}/exercise.json`;
    }

    
    init_definitions(){
	fetch("/static/available_languages.json").
	    then(x => {
		x.json().then( x => {
		    this.declare_available_locales(x);}).
		    then(() =>{
			this.define_language_selector("#switch-language");});
	    });
    }
    
}

class metaLocale extends dynamicLocale{
    
    constructor(){
	super("#db-locale");
    }

    locale_path(locale){
	return `/exercises/metaclasses/${META_NAME}/po/${locale}/exercise.json`;
    }
    
}

class exerciseLocale extends dynamicLocale{
    constructor(){
	super("#db-locale");
    }

    locale_path(locale){
	var url_list=window.location.href.split("/");
	var exercise_folder_name = url_list[url_list.length -2];
	return `/exercises/${exercise_folder_name}/po/${locale}/exercise.json`;
    }
    
}

var control_locale = new controlLocale();

class ConnectionBanner{
    constructor(){
	this.ndots=1;
	var banner = new Banner();
	banner.content.innerHTML="<div id=\"connecting-message\"></div><br/><div id=\"dots\"></div>";
	var connecting_message=banner.content.querySelector("#connecting-message");
	var dots=banner.content.querySelector("#dots");
	control_locale.add_display_message(connecting_message,
					   () => control_locale.i18n.gettext("Connecting to the server"),true);
	
	dots.innerHTML=".";
	let this_aux=this;
	var interval=setInterval(() => {
    	    dots.innerHTML=".".repeat(this_aux.ndots + 1);
    	    this_aux.ndots = (this_aux.ndots + 1) % 10;
	},1000);

	this.stop_promise=banner.stop_promise.then(result=>{
	    clearInterval(interval);
	});
	
	this.stop=banner.stop;
    }
}

function DisconnectionBanner(){
    var banner = new Banner();
    banner.content.innerHTML="<div id=\"connection-closed-message\"></div><br/><div id=\"another-location-message\"></div>";
    var closed_connection = banner.content.querySelector("#connection-closed-message");
    var another_connection = banner.content.querySelector("#another-location-message");
    control_locale.add_display_message(closed_connection,
				       () => control_locale.i18n.gettext("Closed connection."),true);
    control_locale.add_display_message(another_connection,
				       () => control_locale.i18n.gettext("A connection was detected from another location."),true);
    
}

class Timer{
    
    constructor(initial_time,html_object){
	this.initial_time = initial_time;
	this.html_object = html_object;
	this.set_up();
	this.stop_timer_event = new Event('stop');
    }

    set_up(initial_time=null){
	if (initial_time !=null){
	    this.initial_time = initial_time;
	}
	this.remaining_time=this.initial_time;
	this.html_object.innerText = this.seconds_to_clock(this.remaining_time);	
    }

    zero_pad(num, places) {
	return String(num).padStart(places, '0');
    }

    seconds_to_clock(x){
	this.minutes = Math.floor( x / 60);
	this.seconds = x % 60;
	return  this.zero_pad(this.minutes,2) + ":" + this.zero_pad(this.seconds,2) ;
    }

    second_less(){
	this.remaining_time--;
	this.html_object.innerText = this.seconds_to_clock(this.remaining_time);
	if(this.remaining_time == 0){
	    this.html_object.dispatchEvent(this.stop_timer_event);
	}
    }

    stop(event=null){
	clearInterval(this.html_object_interval);
    }

    start(){
	this.html_object_interval= this.setInterval(this.second_less,1000);
	this.html_object.addEventListener('stop',this.stop.bind(this),false);
    }

    setInterval(vCallback, nDelay){
	var oThis = this, aArgs = Array.prototype.slice.call(arguments, 2);
	return setInterval(vCallback instanceof Function ? function () {
	    vCallback.apply(oThis, aArgs);
	} : vCallback, nDelay);
    }


    second_less_synchronized(){
	this.update_counter++;

	if(!(this.update_counter % this.update_rate)){
	    let time_diff = Math.floor((ServerDate.now() - this.starting_time)/1000);
	    this.remaining_time = this.initial_time - time_diff;	    
	}else{
	    this.remaining_time--;	    
	}

	this.html_object.innerText = this.seconds_to_clock(this.remaining_time);
	
	if(this.remaining_time <= 0){
	    this.remaining_time=0;
	    this.html_object.innerText = this.seconds_to_clock(this.remaining_time);
	    this.html_object.dispatchEvent(this.stop_timer_event);
	}
	
    }
    

    start_synchronized(update_rate=5,starting_time=null){
	if (starting_time == null){
	    this.starting_time = ServerDate.now();
	}else{
	    this.starting_time = starting_time;
	}
	this.update_rate=update_rate;
	this.update_counter=0;
	this.html_object_interval = this.setInterval(this.second_less_synchronized,1000);
	this.html_object.addEventListener('stop',this.stop.bind(this),false);
    }

}

class Chronometer{
    
    constructor(html_object){
	this.html_object = html_object;
	this.set_up();
	this.stop_timer_event = new Event('stop');
    }

    set_up(initial_time=0){
	this.time_passed = initial_time;
	this.html_object.innerText = this.seconds_to_clock(this.time_passed);	
    }

    zero_pad(num, places) {
	return String(num).padStart(places, '0');
    }

    seconds_to_clock(x){
	this.minutes = Math.floor( x / 60);
	this.seconds = x % 60;
	return  this.zero_pad(this.minutes,2) + ":" + this.zero_pad(this.seconds,2) ;
    }

    second_more(){
	this.time_passed++;
	this.html_object.innerText = this.seconds_to_clock(this.time_passed);
    }

    
    setInterval(vCallback, nDelay){
	var oThis = this, aArgs = Array.prototype.slice.call(arguments, 2);
	return setInterval(vCallback instanceof Function ? function () {
	    vCallback.apply(oThis, aArgs);
	} : vCallback, nDelay);
    }
    
    start(){
	this.html_object_interval= this.setInterval(this.second_more,1000);
	this.html_object.addEventListener('stop',this.stop.bind(this),false);
    }

    stop(event=null){
	clearInterval(this.html_object_interval);
    }

    second_more_synchronized(){
	this.update_counter++;

	if(!(this.update_counter % this.update_rate)){
	    this.time_passed = Math.floor((ServerDate.now() - this.starting_time)/1000);
	}else{
	    this.time_passed++;
	}

	this.html_object.innerText = this.seconds_to_clock(this.time_passed);
    }

    start_synchronized(update_rate=5,starting_time=null){
	if (starting_time == null){
	    this.starting_time = ServerDate.now();
	}else{
	    this.starting_time = starting_time;
	}
	this.update_rate=update_rate;
	this.update_counter=0;
	this.html_object_interval = this.setInterval(this.second_more_synchronized,1000);
	this.html_object.addEventListener('stop',this.stop.bind(this),false);
    }
   
}






class Exercise{
    
    constructor(){
	if (window.location.protocol == "https:"){
	    this.websocket_url = 'wss://' +  window.location.host + window.location.pathname;
	}else{
	    this.websocket_url = 'ws://' +  window.location.host + window.location.pathname;
	}

	this.showing_banner=false;

	this.upper_panel = document.getElementById("upper-panel");
	this.div = document.getElementById("exercise");
	this.lower_panel = document.getElementById("lower-panel");

	this.results={};

	if(this.process_message == undefined){
	    console.log("Add a process message function to receive messages");
	}

	this.connect_websocket();
	this.start_keybindings();

	if(this.i_am_supervisor()){
	    this.exercise_sync();

	    control_locale.add_display_message(
		document.querySelector("#goto-exercise"),
		() => control_locale.i18n.gettext("Go to exercise"),true);
	    
	}else{
	    this.keep_alive();

	    try{
	    control_locale.add_display_message(
		document.querySelector("#empezar"),
		() => control_locale.i18n.gettext("Start"),true);
	    }catch(err){
		null
	    }

	    try{
	    control_locale.add_display_message(
		document.querySelector("#waiting"),
		() => control_locale.i18n.gettext("Waiting for the supervisor ..."),true);
	    }catch(err){
		null
	    }

	    
	}


	this.control_locale = control_locale;	
	this.meta_locale = undefined;
	this.exercise_locale = undefined;

	this.control_locale.on_language_change = () => {
	    this.message_change_locale(this.control_locale.language_selector.value);
	    document.querySelector("#closed-menu").click();

	    
	    if(this.meta_locale != undefined){
		return this.meta_locale.load_locale(this.control_locale.language_selector.value).then(
		    () => {
			if(this.exercise_locale!=undefined){
			    return this.exercise_locale.load_locale(this.control_locale.language_selector.value);
			}
		    }
		);
	    } else {	    
		if(this.exercise_locale!=undefined){
		    return this.exercise_locale.load_locale(this.control_locale.language_selector.value);
		}
	    }
	};
	
    }
    
    connect_websocket(connection_banner=undefined){

	
	if(! this.showing_banner){
	    this.showing_banner=true;
	    connection_banner = new ConnectionBanner();
	    connection_banner.stop_promise.then(()=>{
		this.showing_banner=false
	    });
	}

	
	this.socket = new WebSocket(this.websocket_url);
	let this_aux = this;
	this.socket.onmessage = function(e){
	    let full_message = JSON.parse(e.data);	    
	    if(full_message.code == 0  ){
		this_aux.process_message(full_message.message);
	    }else if(full_message.code == 1 ){
		this_aux.message_goto();
	    }
	    else if (full_message.code == 2 ){
		if( this_aux.exercise_name() != full_message.exercise ){
		    let key=window.location.pathname.split("/").slice(-1);
		    this_aux.socket.close();
		    window.location.href = window.location.origin + "/" + full_message.exercise + "/" + key;
		}
	    }
	    else if (full_message.code == 3){
		
		if(this_aux.exercise_locale != undefined){
		    this_aux.exercise_locale.load_locale(full_message.locale);
		}
		if(this_aux.meta_locale != undefined){
		    this_aux.meta_locale.load_locale(full_message.locale);
		}
		this_aux.control_locale.load_locale(full_message.locale);
	    }
	    else if (full_message.code == 4){
		// this_aux.
		this_aux.display_exercises(full_message.exercises);
	    }
	};
	
	this.socket.onclose = function(e){
	    var close_seconds = new Date(Date.now()) - this_aux.open_time;	    
	    if(e.code != 3000){
		setTimeout(() => {
		    this_aux.socket=null;
		    this_aux.connect_websocket(connection_banner);} ,1000);
	    }else{
		new DisconnectionBanner();
	    }
	    
	};
	
	this.socket.onopen = function(e){
	    this_aux.open_time=new Date(Date.now());
	    if(connection_banner){
		connection_banner.stop();
	    }
	};
    }

    display_exercises(exercise_locales){
	var all_exercise_links=document.querySelectorAll(".exercise-link")
	all_exercise_links.forEach(x =>{
	    var exercise_folder=x.getAttribute("href").split("/")[1];
	    x.innerHTML = exercise_locales[exercise_folder];
	})
	
    }

    

    start_keybindings(){
	this.keybindins = {};

	let this_aux = this;
	document.onkeydown = function(e){
	    
	    if (e.isComposing || e.keyCode === 229) {
		return;
	    }
	    
	    if(Object.keys(this_aux.keybindins).includes(e.keyCode.toString())){
		this_aux.keybindins[e.keyCode]();
	    }
	    
	};
    }

    add_keybinding(key_code,thunk){
	this.keybindins[key_code]=thunk;	
    }

    send_message(message,who=undefined){
	if(! who){
	    who=this;
	}
	if(who.socket.readyState == WebSocket.OPEN){
	    who.socket.send(message);
	}else{
	    who.connect_websocket();
	    who.socket.send(message);
	}
    }

    message_peer(message){
	if(typeof(message) == "function"){
	    this.send_message(JSON.stringify({"peer" : message()}));
	} else{
	    this.send_message(JSON.stringify({"peer" : message}));
	}
    }

    message_goto(x=null){
	if(x){
	    this.send_message(JSON.stringify({"goto" : x.exercise_name()}),x);
	}else{
	    this.send_message(JSON.stringify({"goto" : this.exercise_name()}));
	}
    }

    message_change_locale(locale){
	this.send_message(  JSON.stringify({"locale" : locale }) );
    }

    message_save_results(results_in_json){
	this.send_message(JSON.stringify({"results" : JSON.stringify(this.results) }));
    }

    keep_alive(){
	let this_aux=this;
	setInterval( () => {this_aux.send_message(JSON.stringify(null));},30000);	
    }

    exercise_sync(){
	let this_aux=this;
	setInterval( ()=>{this_aux.send_message},5000,this);	
    }

    i_am_supervisor(){
	// Returns true when the file being loaded is the supervisor file
	// it only works when the file is first loaded
	return document.currentScript.src.split("/").slice(-1)[0] == "supervisor.js";	
    }

    exercise_name(){
	// Returns the exercise name if the page is an exercise
	// If it is the main page it returns null
	var path_array=window.location.pathname.split("/");
	if(path_array.length ==3){
	    return path_array[1];
	}else{
	    return null;
	}	
    }

    add_timer(time){
	this.timer_text = document.createElement("div");
	this.timer_text.className="time";
	this.timer_text.id="timer";
	this.upper_panel.appendChild(this.timer_text);
	this.time = new Timer(time,this.timer_text);	
    }

    add_chronometer(){
	this.chronometer_text = document.createElement("div");
	this.upper_panel.appendChild(this.chronometer_text);
	this.time = new Chronometer(this.chronometer_text);	
    }

    add_time_control_button(){
	this.time_button = document.createElement("button");
	this.time_button.className="time_button button is-info is-rounded is-normal";
	// this.time_button.classList.add("button","time_button");
	this.time_button.id="time_button";
	this.upper_panel.appendChild(this.time_button);
	this.set_time_start_button();
	this.time_button.disabled=true;
	this.add_keybinding(32,()=>{this.time_button.click();});	
	
    }

    set_time_start_button(){
	control_locale.add_display_message(this.time_button,
					   () => control_locale.i18n.gettext("Start time"),true);
	let aux=this;
	let existing_onkeyup_function=document.onkeyup;
	this.time_button.onclick = function(e){
	    aux.time.start();
	    aux.set_time_stop_button();
	    if(existing_onkeyup_function){
		existing_onkeyup_function(e);
	    };
	};
    }

    set_time_stop_button(){
	control_locale.add_display_message(this.time_button,
					   () => control_locale.i18n.gettext("Stop time"),true);
	let aux=this;
	this.time_button.onclick = function(event){
	    aux.time.stop();
	    aux.set_time_start_button();
	};
    }

    add_errors_counter(){

	this.errors_div = this.add_element("div","errors_div","errors_div",this.lower_panel);
	this.errors_label = this.add_element("span","errors_label","errors_label",this.errors_div);
	this.errors_counter = this.add_element("span","errors_counter","errors_counter",this.errors_div);
	
	// this.errors_label.innerText = "Errores: ";
	control_locale.add_display_message(this.errors_label,
					   () => control_locale.i18n.gettext("Errors: "),true);
	this.errors_counter.innerText = "0";
    }

    add_errors_counter_buttons(){

	this.error_buttons_div = document.createElement("div");
	this.error_buttons_div.className="error_buttons_div";
	this.error_buttons_div.id = "error_buttons_div";
	
	this.error_plus_one_button = document.createElement("button");
	// this.error_plus_one_button.innerText = "Error +1";
	control_locale.add_display_message(this.error_plus_one_button,
					   () => control_locale.i18n.gettext("Error +1"),true);
	this.error_plus_one_button.className = "error_button button is-info is-rounded is-normal";
	this.error_plus_one_button.id="button_plus_error";
	
	this.error_minus_one_button = document.createElement("button");
	// this.error_minus_one_button.innerText = "Error -1";
	control_locale.add_display_message(this.error_minus_one_button,
					   () => control_locale.i18n.gettext("Error -1"),true);
	this.error_minus_one_button.className="error_button button is-info is-rounded is-normal";
	this.error_minus_one_button.id="button_minus_error";

	this.error_buttons_div.appendChild(this.error_minus_one_button);
	this.error_buttons_div.appendChild(this.error_plus_one_button);


	this.lower_panel.appendChild(this.error_buttons_div);

	let aux = this;
	this.error_plus_one_button.onclick = function(e){
	    aux.errors_counter.innerText = Number(aux.errors_counter.innerText) + 1;
	};

	this.error_minus_one_button.onclick = function(e){
	    let current_value = Number(aux.errors_counter.innerText);
	    if(current_value> 0){
		aux.errors_counter.innerText =  current_value - 1;
	    }
	};

	this.add_keybinding(40,()=>{
	    this.error_minus_one_button.click();
	});

	this.add_keybinding(38,()=>{
	    this.error_plus_one_button.click();
	});
	
    }

    add_control_buttons(){
	this.control_buttons_div = document.createElement("div");
	this.control_buttons_div.className = "control_buttons_div";
	this.control_buttons_div.id = "control_buttons_div";

	this.control_button_previous = document.createElement("button");
	this.control_button_previous.className = "control_button button is-info is-rounded is-normal";
	this.control_button_previous.id = "control_button_previous";
	// this.control_button_previous.innerText = i18n.gettext("Previous");
	this.control_locale.add_display_message(this.control_button_previous,() => this.control_locale.i18n.gettext("Previous"),true);
	
	this.control_button_next = document.createElement("button");	
	this.control_button_next.className = "control_button button is-info is-rounded is-normal";
	this.control_button_next.id = "control_button_next";
	// this.control_button_next.innerText = i18n.gettext("Next");
	this.control_locale.add_display_message(this.control_button_next,() => this.control_locale.i18n.gettext("Next"),true );

	this.control_buttons_div.appendChild(this.control_button_previous);
	this.control_buttons_div.appendChild(this.control_button_next);

	this.lower_panel.appendChild(this.control_buttons_div);

	let aux=this;	
	this.control_button_previous.onclick = function(e){
	    aux.previous();
	};

	this.control_button_next.onclick = function(e){
	    aux.next();
	};

	this.add_keybinding(39,()=>{
	    this.next();
	});

	this.add_keybinding(37,()=>{
	    this.previous();
	});
	
    }
		
    add_answer(){
	this.answer_div = document.createElement("div");
	this.answer_label = document.createElement("span");
	control_locale.add_display_message(this.answer_label,
					   () => control_locale.i18n.gettext("Answer: "),
					   true);
	this.answer = document.createElement("span");
	this.answer_div.appendChild(this.answer_label);
	this.answer_div.appendChild(this.answer);
	this.lower_panel.appendChild(this.answer_div);
    }
    
    add_element(type,css_class,id,parent){
	let elem = document.createElement(type);
	elem.className = css_class;
	if(id!=null)
	    elem.id = id;
	parent.appendChild(elem);
	return elem;
    }

    get_random_int(min,max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}


class slidesExercise extends Exercise {
    // Class for exercises that require slides
    // The use must define the following methods in common.js
    // show_slide() - Should receive arguments for showing the next slide.
    // The supervisor must define the methods:
    
    // set_up_cycle(args) - It should define an array with inputs for show_slide and strings

    // Definitions
    // Nslides - Number of slides in a given cycle
    // slide - html element containing the slide information
    // slide_number - Number of the current slide
    // Ncycles - Number of repetitions required by the exercise
    // enumerate_slides - An array whose i-th entry contains the arguments to pass to the update method for the (i+1)-th slide.
    // cycle - The number of the current repetition. When the class is instantiated it is set to zero.
    // results - An object containing the time and number of errors (if they exist);
    

    //min_slide is the slide from which the clock will start. It can
    //be used to show information at the beggining of an excercise.

    //credits are the credits present in the info.json (when it
    //exists) of the exercise.
    
    constructor(){	
	super();
	this.message = this.add_element("div","","message",this.div);	
	this.slide = this.add_element("div","slide","slide",this.div);
	if(this.i_am_supervisor()){
	    this.slide_number_label = this.add_element("div","slide-number","slide-number",this.lower_panel);
	}
	this.cycle = 0;
	this.display_answer=false;

	// The only person of the following line is for the phrases to appear in the po file
	var start_message_locale_aux=[this.control_locale.i18n.gettext("First repetition"),
				      this.control_locale.i18n.gettext("Second repetition"),
				      this.control_locale.i18n.gettext("Third repetition")];
	
	this.start_message=["First repetition",
			    "Second repetition",
			    "Third repetition"];



	this.start_next_cycle()

	this.min_slide = 0;
	if(this.credits==undefined){
	    this.credits=false;
	}
	
	// this.get_credits();
	
    }

    update_slide_label(n){
	if(this.slide_number_label != undefined){
	    this.slide_number_label.innerHTML=`${n - this.min_slide + 1 }/${this.Nslides - this.min_slide  }`;
	}
    }

    add_controls(chronometer=true,answer=true,errors=true){
	
	if(chronometer){
	    this.add_chronometer();
	    this.add_time_control_button();
	    this.results["time"]=[];
	}
	
	if(answer){
	    this.display_answer=true;
	    this.add_answer();
	    if(errors){
		this.add_errors_counter();
	    }
	}

	this.add_control_buttons();

	if(errors){
	    this.add_errors_counter_buttons();
	    this.results["errors"]=[];
	}
	
    }

    finish_set_up_cycle(){
	this.show_message(this.start_message[this.cycle -1]);	
	if(this.slide_number_label != undefined){
	    this.slide_number_label.innerHTML="";
	}
	this.Nslides = this.enumerate_slides.length;
	if(this.answer!=undefined)
	    this.answer.innerHTML="";
    }

    set_up_cycle_wrapper(){
	if (this.set_up_cycle.constructor.name == "AsyncFunction" ){
	    this.set_up_cycle().then(() =>{
		this.finish_set_up_cycle();
	    })
	}else{
	    this.set_up_cycle();
	    this.finish_set_up_cycle();
	}
    }

    start_next_cycle(){
	this.cycle++;
	this.slide_number=0;
	if(this.init_vars!=undefined)
	    this.init_vars();
	

	if(this.exercise_locale != undefined){
	    this.exercise_locale.finished_loading.then(() => {
		this.set_up_cycle_wrapper();
	    });
	}else{	    
	    this.set_up_cycle_wrapper();
	}

    }
    

    show_message(message){
	this.disappear_slide();
	this.message.style.display="";
	// this.message.innerHTML=message;
	if( typeof(message) === "string"  ){
	    this.control_locale.add_display_message(this.message,() => this.control_locale.i18n.gettext(message),true);
	} else {
	    this.control_locale.add_display_message(this.message, message,true);
	}
    }


    disappear_message(){
	this.message.style.display="none";
    }

    disappear_slide(){
	this.slide.style.display="none";
    }

    update(message_or_slide){
	if (typeof(message_or_slide) === "string" || typeof(message_or_slide) === "function" ){
	    this.show_message(message_or_slide);
	    if(this.slide_number_label != undefined){
		this.slide_number_label.innerHTML="";
	    }
	}else{
	    this.disappear_message();
	    this.slide.style.display="";
	    this.show_slide(...message_or_slide);	    
	}
    }

    update_results(){
	if(this.results.time != undefined){	    
	    this.results.time.push(this.time.html_object.innerText);
	    this.time.set_up();
	}
	if(this.results.errors != undefined){
	    this.results.errors.push(this.errors_counter.innerText);
	    this.errors_counter.innerText=0;
	}
    }

    add_results_row(parent,time=null,errors=null){
	let row=this.add_element("tr","results_row",null,parent);
	if(time!=null){
	    let time_element = this.add_element("td","results_cell",null,row);
	    time_element.innerText = time;
	}
	if(errors!=null){
	    let errors_element = this.add_element("td","results_cell",null,row);
	    errors_element.innerText = errors;
	}
	
    }

    show_results(){
	this.upper_panel.style.display="none";
	this.lower_panel.style.display="none";
	this.disappear_message();
	this.disappear_slide();
	this.results_table=this.add_element("table","results_table table","results_table",this.div);
	this.headers_row = this.add_element("tr","results_table_header_row","results_table_header_row",this.results_table);
	if(this.results.time != undefined){
	    let time_header = this.add_element("th","results_header","header_time",this.headers_row);
	    control_locale.add_display_message(time_header,
					       () => control_locale.i18n.gettext("Time"),true);
	}
	if(this.results.errors != undefined){
	    let errors_header = this.add_element("th","results_header","header_errors",this.headers_row);
	    control_locale.add_display_message(errors_header,
					       () => control_locale.i18n.gettext("Errors"),true);
	    
	}

	for(let i=0;i< this.Ncycles; i++){

	    if( this.results.time != undefined &&  this.results.errors != undefined){
		this.add_results_row(this.results_table,this.results.time[i],this.results.errors[i]);
	    }else if (this.results.time != undefined ){
		
		this.add_results_row(this.results_table,this.results.time[i]);
	    }else if (this.results.errors != undefined){
		this.add_results_row(this.results_table,null,this.results.errors[i]);
	    }		
	}
	this.add_element("br","","",this.div);
	this.save_results_button=this.add_element("button","button is-info","",this.div);
	control_locale.add_display_message(this.save_results_button,
					       () => control_locale.i18n.gettext("Save results"),true);
	this.save_results_button.style.margin="auto";
	this.save_results_button.style.display="block";

	let this_aux=this;
	this.save_results_button.onclick=function(e){
	    this_aux.message_save_results();
	    this_aux.save_results_button.remove();
	};
	
	    
    }


    next(){
	if(this.slide_number < this.Nslides){
	    this.message_peer(this.enumerate_slides[this.slide_number]);
	    if(this.slide_number==this.min_slide){
		this.time_button.disabled=false;
		this.time.start();
		this.set_time_stop_button();
	    }
	    
	    this.update(this.enumerate_slides[this.slide_number]);
	    this.update_slide_label(this.slide_number);
	    this.update_answer(this.slide_number);
	    this.slide_number++;
	}else if (this.slide_number == this.Nslides){
	    if(this.cycle < this.Ncycles){
		var done_message = control_locale.i18n.gettext("Done!");
		this.show_message(
		    () => {
			this.message_peer(done_message);
			return done_message}
		);
		this.time.stop();
		this.set_time_start_button();
		this.time_button.disabled=true;
	    }else{
		if(this.credits){
		    
		    var finished = ()=>{
			var finished_message= control_locale.i18n.gettext("We finished!");
			var credits = control_locale.i18n.gettext("Credits");
			if(this.exercise_locale != undefined){
			    var credits_message = this.exercise_locale.i18n.gettext(this.credits);
			}else{
			    var credits_message = this.credits;
			}
			
			var full_message = `${finished_message}<br/><br/><div class="is-size-5">${credits_message}</div>`;
			this.message_peer(full_message);
			return full_message;
		    }
		    
		}else{
		    var finished = () => {
			var finished_message= control_locale.i18n.gettext("We finished!");
			var full_message = finished_message;
			this.message_peer(full_message);
			return full_message;
		    }
		}
		this.show_message(finished);
		this.time.stop();
	    }
	    this.slide_number++;
	}else{
	    this.update_results();
	    if(this.cycle < this.Ncycles){
		if(this.cycle < this.Ncycles)
		    this.start_next_cycle();
	    }else{
		this.show_results();
		this.keybindins={};
	    }
	}
    }

    previous(){
	if(this.slide_number > 1){
	    this.slide_number--;
	    if(this.slide_number==this.min_slide){
		this.time.stop();
		this.set_time_start_button();
		this.time_button.disabled=true;
	    }
	    this.message_peer(this.enumerate_slides[this.slide_number-1]);
	    this.update(this.enumerate_slides[this.slide_number-1]);
	    this.update_slide_label(this.slide_number-1);
	    this.update_answer(this.slide_number-1);
	}
    }

    set_up_cycle(){
    	this.enumerate_slides=[];
    }

    update_answer(index){
	if(this.answer!=undefined){
	    if(this.exercise_locale != undefined){
		this.exercise_locale.add_display_message(this.answer,() => this.get_answer(index),true);
	    }else{
		this.answer.innerHTML = this.get_answer(index);
	    }
	}
	else{
	    if(this.get_answer != undefined)
		this.get_answer(index);
	}
    }

}

function loadScript(src){        
    return new Promise(function(resolve,reject){
	let new_script = document.createElement("script");
	new_script.src=src;
	new_script.onload = () => resolve(1);
	new_script.onerror = () => reject(new Error(`Error loading $(src)`));
	document.head.append(new_script);
    });
}

function metaCommonPath(meta_name){
    return `/exercises/metaclasses/${meta_name}/common.js`;
}

function metaSupervisorPath(meta_name){
    return `/exercises/metaclasses/${meta_name}/supervisor.js`;
}

function metaPatientPath(meta_name){
    return `/exercises/metaclasses/${meta_name}/patient.js`;
}

function metaStylePath(meta_name){
    return `/exercises/metaclasses/${meta_name}/style.css`;
}

function loadMetaStyle(meta_name){
    return new Promise(function(resolve,reject){
	var style=document.createElement("link");
	style.rel="stylesheet";
	style.type="text/css";
	style.href=metaStylePath(meta_name);
	style.onload = () => resolve(1);			      
	style.onerror = () => reject(new Error(`Error loading $(src)`));
	document.head.append(style);
    });
}

function loadMetaRole(meta_name,
		  load_common,
		  load_style,		  
		  start_function,
		  supervisor=false){
    // meta_name : A string with the name of the folder in which the meta class is defined
    // load_common : If true the common.js from the meta folder is loaded before  running supervisor.js in the meta folder
    // start_function : A function that creates a child of the supervisor metaclass and instantiates it to start the exercise
    META_NAME=meta_name;
    var common_promise=undefined;
    var style_promise=undefined;
    if(load_common){
	common_promise = loadScript(metaCommonPath(meta_name));
    }else {
	common_promise = new Promise((resolve,reject) => resolve(1));
    }

    if(load_style){
	style_promise = loadMetaStyle(meta_name);
    }else{
	style_promise = new Promise((resolve,reject) => resolve(1));
    }

    return style_promise.then( () => {
	return common_promise;
    })
	.then( () => {
	    if(supervisor){
		return loadScript(metaSupervisorPath(meta_name));
	    }else{
		return loadScript(metaPatientPath(meta_name));
	    }
	})	
	.then( () => {
	    start_function();
	});		
}
		 

function loadMetaSupervisor(meta_name,
			    load_common,
			    load_style,
			    start_function
			   ){
    loadMetaRole(meta_name,load_common,load_style,start_function,true);

}


function loadMetaPatient(meta_name,
			 load_common,
			 load_style,
			 start_function){
    loadMetaRole(meta_name,load_common,load_style,start_function,false);
}
			    
