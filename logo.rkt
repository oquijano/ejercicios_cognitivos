;; 
;; Okom - A platform of supervised cognitive exercises
;; Copyright (C) 2023 Oscar Alberto Quijano Xacur
;;
;; This file is part of Okom.
;;
;; Okom is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; Okom is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Okom.  If not, see <http://www.gnu.org/licenses/>.
;; 
#lang racket/base

(require 2htdp/image
	 threading
	 (for-syntax racket/base
		     syntax/parse)
	 syntax/parse/define
	 racket/math
	 infix)

;; Constants used for logo 
;; (define WIDTH 200)
;; (define HEIGHT 200)
;; (define INITIAL-LENGTH 45)
;; (define ANGLE-IN-DEGREES 25)
;; (define NEXT-BRANCH-PROPORTION 0.75)
;; (define WIDTH-LENGTH-PROPORTION 0.15)
;; and then (draw-tree 10)

;; Independent constants
(define WIDTH 200)
(define HEIGHT 200)
(define INITIAL-LENGTH 45)
(define ANGLE-IN-DEGREES 25)
(define NEXT-BRANCH-PROPORTION 0.75)
(define WIDTH-LENGTH-PROPORTION 0.15)

;; Dependent constants
(define ANGLE ($ "ANGLE_IN_DEGREES * pi / 180") )

(define (tree-pen width
		  #:style [style 'solid]
		  #:cap [cap 'round]
		  #:join [join 'round]) 
  (pen "black" width style cap join))




(define (perpendicular p)
  (make-rectangular (- (imag-part p)) (real-part p)))


(define (transform-coords p)
  (make-rectangular
   ($ "real_part[p] + WIDTH/2")
   ($ "HEIGHT - imag_part[p]")))

(define (draw-line p1 p2 [current-image (empty-scene WIDTH HEIGHT)])
  
  (define current-length (magnitude ($ "p2-p1")))
  (define current-width ($ "max[ceiling[WIDTH_LENGTH_PROPORTION * current_length],1]"))

  (let ([tp1 (transform-coords p1)]
	[tp2 (transform-coords p2)])
    (add-line current-image
	    (real-part tp1) (imag-part tp1)
	    (real-part tp2) (imag-part tp2)
	    (tree-pen current-width))
    )
  )

(define (draw-linel points-list an-image)
  (draw-line (car points-list)
	     (cadr points-list)
	     an-image)
  )

(define (next-lines p1 p2)
  (define part1 ($ "p2 + (p2-p1) * NEXT_BRANCH_PROPORTION*cos[ANGLE]") )
  (define part2 ($ "NEXT_BRANCH_PROPORTION * abs[sin[ANGLE]]* perpendicular[p2-p1]"))

  (define next-point1 ($ "part1 - part2"))
  (define next-point2 ($ "part1 + part2"))

  (list
   (list p2 next-point1)
   (list p2 next-point2)))

(define (draw-tree N)
  (let loop ([lines '()]
	     [image (empty-scene WIDTH HEIGHT)]
	     [i N])
    (cond
     [(= i 0) image]
     [(null? lines)
      (let ([p1 0]
	    [p2 (make-rectangular 0  INITIAL-LENGTH)])
       (loop
	(list (list p1 p2))
	(draw-line p1 p2 image)
	(sub1  i)))]
     [else
      (let ([all-lines (foldl append '()  (map (λ (x) (apply next-lines x)) lines ))])
	(loop
	 all-lines
	 (for/fold ([cur-image image]) ([cur-line all-lines])
	   (draw-line (car cur-line) (cadr cur-line) cur-image ))
	 (sub1 i)	      
	 ))])))


(module+ icons
  
  (save-svg-image (draw-tree 10)
		  "static/okom.svg")
  
  (save-image (draw-tree 10)
	      "static/okom_16x16.png"
	      16 16)

  (save-image (draw-tree 10)
	      "static/okom_32x32.png"
	      32 32)
  
  (save-image (draw-tree 10)
	      "static/okom_180x180.png"
	      180 180)

  (save-image (draw-tree 10)
	      "static/okom_192x192.png"
	      192 192)

  
  )




(module+ test
  (require 2htdp/universe)
  
  (run-movie
   0.5
   (for/list ([x (in-range 12)]) (draw-tree x)))

  )

(module+ save_images
  
  (for/list ([x (in-range 13)])
    (save-svg-image
     (draw-tree x)
     (format "tree_~a.svg" x)))
  
  )

