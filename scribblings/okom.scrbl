#lang scribble/manual

@(require scribble/core
	  scribble/html-properties
	  okom/localization
	  (only-in xml cdata))

@require[@for-label[okom
		    okom/localization
		    racket/logging
                    racket/base]]


@(define prism-css
   (make-style "language-js"
	       (list (make-css-addition "prism_files/prism.css")
	       	      (make-js-addition "prism_files/prism.js"))))
	       	       	       
@elem[#:style prism-css]

@(define js-code (xexpr-property
		  (cdata #f #f "<pre><code class=\"language-js\">")
		  (cdata #f #f "</code></pre>")))

@(define (js x)
  (elem #:style (style #f (list js-code)) x))
		  
		  
@(define prism-js (xexpr-property
		  (cdata #f #f "<script src=\"prism_files/prism.js\">")
		  (cdata #f #f "</script>")))


@title{okom}
@author{oscar}

@defmodule[okom]

Supervised cognitive exercises platform.

@section{Messages between parties}

Each exercise should have the option to save time, errors and comments.

Each message from the clients should be a json object containing one name,
either @racket['peer] or @racket['server], depending on whether the message
should be passed to the peer or processed by the server.

@section{Messages from the server}

All messages from the server will contain the name @racket['code] containing an
integer with the following meanings.

@itemlist[@item{@bold{0} Message with contents. The object also comes with the name @racket['message] with the payload}
	  @item{@bold{1} The peer has not connected yet. No action is currently implemented when this code is received.}
	  @item{@bold{2} Go to another exercises. The object also comes with the key @racket['exercise-name] telling what exercise to go to.}
	  @item{@bold{3} Change locale. The object also comes with the key @racket['locale] telling what locale to use.}
	  @item{@bold{4} Exercises list. The object also comes with the key @racket['exercises] containing a javascript object whose keys are exercise folders and the respective values are the display strings for each exercise}
	  ]

@section{Adding new exercises}

Exercises can be added without writing one line of racket, javascript is used
for this.

Each exercise consists of a folder containing javascript files
@bold{supervisor.js} and @bold{patient.js}. The first one is loaded in
the supervisor side while the second one is loaded on the patient
side. There are also two additional files that can optionally be included (either one or the
other or both), @racket{common.js} and @bold{style.css}.If present, the file
@racket{common.js} is included on both sides (supervisor and
patient). Similarly, if present, the css file @bold{style.css} will be loaded on
both sides.

All exercises load a javascript file called @bold{exercise.js}, which
contains a class called @racket['Exercise]. It contains utilities and methods
providing features that are common to many exercises. Both sides (supervisor
and patient) should instantiate this class. We divide the methods of this class
into four categories: communication among peers, time measurement

@subsection{Methods for Communication Among Peers}

With peers we mean a supervisor and a patient. Communication among peers is
necessary for the actions of one side to have an effect on the other
side. The methods related for peer communication are the following:

@itemlist[
@item{@bold{message_peer()} Takes an object as an argument and sends it to the
peer. In other words, if used on the patient side it sends the object
to the supervisor and if run on the supervisor side it sends the
message to the patient}

@item{@bold{process_message(message)} This method is not provided by the class
@racket['Exercise], it has to be defined by child classes. It should define the
actions to be taken when a message is received.}
]

@subsection{Methods for Time Measurement}

Some exercises require a limited amount of time, the class provides a timer for
these cases. Other exercises require to keep a record of the time required to
solve an exercise. The class provides a chronometer for these cases. The class
assumes that only one of these will be used. If you add more than one of them,
only the last one will be controlled by the start and stop methods shown
here (In a later section we discuss how to add more than one, in case an
exercise requires it). 


@itemlist[

@item{@bold{add_timer(time)} Adds a @bold{time} seconds timer to the app, with
the format mm:ss. After calling this function, the corresponding HTML object
with the text of the timer can be accessed with the field
@italic{timer_text}. It's css class is @italic{time} and it's html id is
@italic{timer}.}

@item{@bold{add_chronometer()} Adds a chronometer with the format mm:ss. The
corresponding HTMl object can be accessed with the field
@italic{chronometer_text}. }

]

When either a timer or a chronometer is added, it is saved in the field
@racket['time], which contains the following methods:

@itemlist[

@item{@bold{time.start()} Starts the chronometer or timer.}

@item{@bold{time.stop()} Stops the chronometer or timer.}

]

The class also provides a method to add a button for managing time.

@itemlist[
@item{@bold{add_time_control_button()} Creates a button to start and stop
either the timer or the chronometer. It also adds the keyboard shortcut
@italic{space} which triggers the button.}
]


@subsection{Methods for Error Counting}

It is common for exercises to require to keep a record on the number of errors
made by the patient during the exercises. The class provides inputs and buttons
to facilitate this.

@itemlist[

@item{@bold{add_errors_counter()} Adds a numeric input field for counting
errors with a label and a label for it. Both are contained in a div element. It
also adds to keyboard shortcuts, the up arrow increases the error count while
the down arrow decreases it.}

@item{@bold{add_errors_counter_buttons()} Adds a pair of buttons, for
increasing and decreasing the number of errors.}

]

@subsection{Methods for Controlling an Exercise}

Many exercises have different stages or slides that the supervisor goes
through. For this cases the class created (which is a subclass of
@racket['Exercise]) by the user should contain the methods @bold{next} and
@bold{previous}. Both methods are intended to be used on the supervisor
side. @bold{next} should go to the next slide locally and send a message to the
patient side so the slide changes there too. Similarly @bold{previous} should
go to the previous slide locally and send a message to the peer so it changes
on the patient side.


@itemlist[

@item{@bold{add_control_buttons()} Adds two buttons. One calls the method
@bold{previous} and the other one the method @bold{next}. It also adds two
keyboard shortcuts. The @italic{right arrow} calls next while the @italic{left
arrow} calls previous.}

]


@subsection{Methods for Checking Answers}


@tabular[#:style 'boxed #:sep @hspace[3] #:row-properties '(bottom-border )  #:column-properties '(center)
(list (list @bold{Description} @bold{JS Object Field Name} @bold{CSS Class Name} @bold{HTML Id}  @bold{Added by method})
      (list "Timer"  "timer_text"  "time" "timer" "add_timer")
      (list "Chronometer" "chronometer_text" "time" "chronometer"  "add_chronometer")
      (list "Time Control Button" "time_button" "time_button"  "time_button" "add_time_control_button")
      (list "Error counter" "errors_counter" "errors_counter" "errors_counter" "add_errors_counter")
      (list "Error counter label" "errors_label"  "errors_label" "errors_label" "add_errors_counter")
      (list "Error counter div" "errors_div" "errors_div" "errors_div" "add_errors_counter")
      (list "Increase error count button" "error_plus_one_button" "error_button" "button_plus_error" "add_errors_counter_buttons")
      (list "Decrease error count button" "error_minus_one_button" "error_button" "button_minus_error" "add_errors_counter_buttons")
      (list "Next button" "control_button_next" "control_button" "control_button_next" "add_control_buttons"  )
      (list "Previous button" "control_button_previous" "control_button" "control_button_previous" "add_errors_counter_buttons")
      )
]

@subsection{Other Methods Provided by the Class}


@itemlist[

@item{@bold{add_element(type,css_class,id,parent)} Adds a new html
element. @bold{type}, @bold{css_class} and @bold{id} should be strings
containing the html type, css class name and html if of the new
element. @bold{parent} should be a javascript object containing the parent
html element.}

@item{@bold{add_keybinding(key_code,thunk)} Adds a new keybinding to the html
document. @bold{key_code} should be an integer containing the key code of the
key that will trigger an action. @bold{thunk} should be a function that takes
no arguments and performs the desired action after the key is pressed.}

@item{@bold{get_random_int(min,max)} Gets a random integer between (and
including) the integers @bold{min} and @bold{max}.}

]

@section{Adding exercises with slides}

Many exercises work like slides. A class called @racket['slidesExercise], which
is a subclass of @racket['Exercise] is provided to make it easy to work with
slides-like exercises. The class contains functionality to change a slide, show
messages between slides, display the correct answer to the supervisor and
repeat the exercise a predetermined number of times.

Often exercises are repeated a few times. We refer to each repetition as a
@italic{cycle}. Each cycle consists of a group of slides and the time and
number of mistakes is tracked independently for each cycle.

Each slide is either a message or a problem. Messages are used to show the
beginning and the end of the slides or between slides to indicate some change
in the problems. Problems are slides for which the patient must give an answer.

One needs to first find a fixed set of parameters that generate a problem. From
now on we will refer to them as @italic{problem parameters}. For example in
the colors exercise each problem consist of two color boxes and a black circle in
one of them; three parameters are used in this case: the color of the first
box, the color of the second box and a number that that represents whether the
black circle should be on the first box or the second one.

To create a new exercise with slides one create a new folder with the
name of the exercise with the files @bold{common.js},
@bold{supervisor.js} and @bold{patient.js}.

Optionally a file called @bold{style.css} can be added, containing css
code for the exercise.

@bold{name.json} is another optional file one should be aware of. It
should contain a json object whose keys are locales and its values are
the name of the exercise in that locale. This name is shown in two
places: For the doctor it appears in the patient page where exercises
are assigned, it also appears in the supervisor page to choose an
exercise. If the file @bold{name.json} does not exist the folder name
of the exercise is used.

@bold{common.js} should contain a child class of @racket{slidesExercise}. It must
contain a @racket{constructor} method that calls @racket{super} along with any
other variable definitions necessary for the class. It must also
have a method called @racket{show_slide} which should receive the problem
parameters and display the corresponding problem on the website.

@bold{supervisor.js} should contain a child of the class defined in
@racket{common.js}. It must contain a @racket{constructor} and two additional
methods: @racket{set_up_cycle} and @racket{get_answer}.

The @racket{constructor} method should call @racket{super} and
@racket{add_controls} and define the fields @racket{Ncycles} and
@racket{min_slide}. @racket{Ncycles} is the number of cycles that the exercise
has and @racket{min_slide} defines the slide on which the clock will start
running automatically (if you do not define it, the clock will start running in
the slide 0 of @racket{enumerate_slides}).

The purpose of @racket{set_up_cycle} is to generate the slides of a cycle. It
should do this by creating a field called @racket{enumerate_slides} which
should be an array; The i-th element of the array contains information that
represents the i-th slide. Each element should be a string or an array. If it
is a string it means that the corresponding slide should show a message with
the contents of that string. If it is an array, it should contain problem
parameters; the method @racket{show_slide} is called with those parameters
to display the corresponding problem. @racket{set_up_cycle} may use the field
@racket['cycle] when defining the slides. When the patient does the exercise
for the first time in a session its value is 1, the second time it is 2 and so
on. The value is defined and updated automatically in @racket{slidesExercise}.

@bold{IMPORTANT:} Note that @racket{set_up_cycle()} is called in the first line
of the constructor as part of @racket{super()}, then @racket{set_up_cycle()}
cannot depend on fields defined in the constructor. If you need to define some
fields and the beginning for them to be available for @racket{set_up_cycle()}
you can define a method called @racket{init_vars()} and define the fields in
there. When this method exists, it is called before @racket{set_up_cycle()}
while @racket{super()} is running.

The purpose of @racket{get_answer} is to show the answer of a slide to the
supervisor. It should receive an integer N and return the answer of the slide
whose parameters are the N-th element of @racket{this.enumerate_slides}.

Similarly to @bold{supervisor.js}, @bold{patient.js} should contain a child of
the class defined in @racket{common.js}. It must contain a @racket{constructor}
method that calls @racket{super} and an additional method called
@racket{process_message}, which should have on argument and simply call
@racket{this.update} on that parameter.

@subsection{Methods provided by @racket['slidesExercise]}

@itemlist[

@item{@bold{add_controls(chronometer=true,answer=true,errors=true)} It should
be used on the supervisor side to add a timer, answer or error counting,
depending on the values of the parameters @bold{chronometer}, @bold{answer} and
@bold{errors} respectively. They are all true by default.}

]


@section{Example}

We implement here an exercise in which numbers are attached to specific
symbols. At the beginning a slide is shown to the patient in which a
correspondence is shown between a group of symbols and the numbers from 0 to
9. The patient is allowed to write down the symbols and their corresponding
numbers.  Then slides are shown one by one, each of them containing a symbol
for which the patient has to say what is the corresponding number. The
supervisor should keep note of the time needed to go through all the slides and
the number of errors made by the patient.



@tabular[ #:style 'centered
          #:column-properties '(border)
(list
    (list "%" "Δ" "↑" "∑" "≠" "∞" "⌂" "⊩" "☼" "≫" )
    (list "1" "2" "3" "4" "5" "6" "7" "8" "9" "0"))
]



@js{
var x = 5;
}


@subsection{File common.js}

@js{@list{
"use strict";

class Symbols extends slidesExercise{

    constructor(){
	super();

	this.slide.innerHTML=@literal{`}
        <div class="symbol" id="symbol">
        </div>@literal{`};

	this.current = document.getElementById("symbol");

	this.symbols = ["&#8811;","%","&#916;","&#8593;","&#8721;","&#8800;","&#8734;","&#8962;","&#8873;","&#9788;"];

	this.symbols_table = @literal{`}<table style="font-size: 50%; margin-left: auto; margin-right: auto ; height: 50%;">
  <tr>
    <td style="border: 1px solid black;"><p>%</p></td>
    <td style="border: 1px solid black;"><p>&#916;</p></td>
    <td style="border: 1px solid black;"><p>&#8593;</p></td>
    <td style="border: 1px solid black;"><p>&#8721;</p></td>
    <td style="border: 1px solid black;"><p>&#8800;</p></td>
    <td style="border: 1px solid black;"><p>&#8734;</p></td>
    <td style="border: 1px solid black;"><p>&#8962;</p></td>
    <td style="border: 1px solid black;"><p>&#8873;</p></td>
    <td style="border: 1px solid black;"><p>&#9788;</p></td>
    <td style="border: 1px solid black;"><p>&#8811;</p></td>
  </tr>
  <tr>
    <td style="border: 1px solid black;"><p>1</p></td>
    <td style="border: 1px solid black;"><p>2</p></td>
    <td style="border: 1px solid black;"><p>3</p></td>    
    <td style="border: 1px solid black;"><p>4</p></td>
    <td style="border: 1px solid black;"><p>5</p></td>
    <td style="border: 1px solid black;"><p>6</p></td>
    <td style="border: 1px solid black;"><p>7</p></td>
    <td style="border: 1px solid black;"><p>8</p></td>
    <td style="border: 1px solid black;"><p>9</p></td>
    <td style="border: 1px solid black;"><p>0</p></td>
  </tr>
</table>@literal{`};	
    }

    show_slide(number){	
	if(number == -1){
	    this.current.innerHTML = this.symbols_table;
	}
	else{
	    this.current.innerHTML = this.symbols[number];
	}
    }
    
}

}
}

@section{Metaclasses}

Sometimes exercises differ only a little bit and it is worth having a javascript
metaclass instead of having copies of the same code in several folders. For
this purpose one can use a folder called @bold{metaclasses} in the exercises
folder. There should be a subfolder for each metaclass. The subfolder name is
the name of the metaclass. Similarly to regular exercise folders it should
contain the files @bold{supervisor.js} and @bold{patient.js} and optionally
@bold{common.js} and @bold{style.css}.

@bold{supervisor.js} and @bold{patient.js} should define metaclasses for the
supervisor and patient respectively. These metaclasses can depend on code
included in the @bold{common.js} file, and css can also be included in the file
@bold{style.css}.

After files in a metaclass are ready, they need to be used in exercises. Inside
of the exercise folder (we are not talking about the metaclass folder any
more), the @bold{supervisor.js} and @bold{patient.js} files should use the
functions @racket{loadMetaSupervisor} and @racket{loadMetaPatient},
respectively. Both functions receive four arguments:

@itemlist[
@item{@bold{meta_name} A string with the name of the metaclass, i.e. the name
of the subfolder in the metaclass folder.}

@item{@bold{load_common} It should be @racket{true} when the metaclass folder
contains a @bold{common.js} file and it should be @racket{false} otherwise.}

@item{@bold{load_style} It should be @racket{true} when the metaclass folder
contains a @bold{style.css} file and it should be @racket{false} otherwise.
}

@item{@bold{start_function} A function with no arguments in which one creates a
child of the metaclass and instantiates it to start the exercise}
]

@bold{IMPORTANT:} If you have a @bold{style.css} file in both the metaclass and
the exercise folder, the one in the exercise folder is loaded before the one in
the metaclass, thus you should not try to rewrite a css class option in the
exercise css file (if that is necessary do it with javascript instead).

An example of a metaclass is provided by the @bold{uno_sin_pareja} exercise in
in 
@hyperlink["https://gitlab.com/oquijano/okom_library/-/tree/master"
"the main exercises repository"].


@section{Logging}

@defmodule[okom/logging]

@defparam[log-output x (or/c output-port? path-string?)]{Sets the output of
the main logger of the app. It can either be an output port, a path or a string
path to a file. The default value is the standard output}

@defthing[main-logger logger?]{Parent logger for all loggers in the library.}

@defparam[log-rcvr x log-level/c]{Main log receiver of the library. The argument
should be a log level, initially it is @racket['debug]}

@defproc[(start-logging) thread?]{Logs events from @racket[log-rcvr] to the
output defined in @racket[log-output].}

Suppose you would like to create a logger of events with topic
@racket['example] with level @racket['info] and that you want the log to be
written to the file @racket{/tmp/app.log}. It can be done in the following
way:

@codeblock|{
#lang racket

(require okom/logging)
(log-output "/tmp/app.log")
(start-logging)

(define my-logger (make-logger 'example main-logger 'info))
(current-logger my-logger)

(log-debug "This does not get logged.")
(log-info "This gets logged")
(log-error "And this too")
}|

@section{Localization}

The platform is multilingual, it currently supports English, Spanish
and French and it allows to add more languages.

Two localization systems are used, one for the static pages (the
doctor platform) and another one for the dynamic pages (the
exercises).

@subsection{Static pages localization (doctor pages)}

Localization for the doctor platform is done using
@hyperlink["https://srfi.schemers.org/srfi-29/srfi-29.html" "SRFI
29"]. The language bundles are in the folder @bold{bundles}. The
filename of each bundle has to be the locale followed by @bold{.rkt}.

After creating the bundle file and adding it to the @bold{bundles}
folder with the name @italic{locale}@bold{.rkt} one only needs to add
and entry to the hash table @bold{display-language-hash} in the
@bold{localization.rkt} file. There should be a string with the locale
code and the value the name of the corresponding language. The latter
is the label shown to the user to choose that language.

@subsection{Dynamic pages localization (exercise pages)}

For exercise pages the library
@hyperlink["https://guillaumepotier.github.io/gettext.js/"
"gettext.js"] in combination with
@hyperlink["https://gitlab.com/oquijano/dynamiclocale"
"dynamicLocale"].

There are three dynamicLocale child classes used for translating
exercises: @racket['controlLocale], @racket['metaLocale] and
@racket['exerciseLocale]. In most cases one needs to work only with
the last one.

@itemlist[

@item{@racket['controlLocale] takes care of translating the control
items used in exercises, e.g. the next and previous slide buttons. It
is always instantiated in the field @italic{control_locale} of the
exercise class.}

@item{@racket['exerciseLocale] takes care of translating elements
present in an exercise. If an exercise requires this class, it should
be instantiated and assigned to the field @italic{exercise_locale} of
the exercise class.}

@item{@racket['metaLocale] If there are elements of an exercise
metaclass that need translation, this is the class that should be used
for them. If an exercise requires this class, it should
be instantiated and assigned to the field @italic{meta_locale} of
the exercise class.}

]

New exercises do not instantiate @racket['controlLocale] since those
translations are already taken core by the base code of the
project. If an exercise uses a metaclass and translations are needed
for html elements defined in the metaclass, then @racket['metaLocale]
should be instantiated for the translations of those elements. For
html elements displayed in an exercise that are not part of a
metaclass and instance of @racket['exerciseLocale] should be used.



@elem[#:style (style #f (list prism-js))]{}
@; LocalWords:  javascript Metaclasses metaclass metaclasses subfolder
