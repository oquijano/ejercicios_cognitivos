Okom
====

[Racket](https://racket-lang.org/) package for hosting a platform of
cognitive exercises.

## Installation

To install okom use the `raco` tool that is installed along racket

```sh
raco pkg install https://gitlab.com/oquijano/okom.git
```

To be able to run okom you need to add the racket packages bin folder
to your PATH environment variable. The following command gives you the
path to that folder.

```sh
racket -e "(path->string (build-path (find-system-path 'addon-dir) (version) \"bin\"))"
```

You can see the built-in help by running 

```
okom -h
```
## Getting Started


To start with using okom you need to create a new folder that will be
used to store the files and templates that program serves. In this
section we use a folder called `okom_tutorial` for the examples. You
can create the folder with the command

```
mkdir okom_tutorial
```

Okom needs two parameters, a folder containing the files and templates
to be served and the port where they are served. The folder can either
be defined in the environment variable `OKOM_PATH` or with the option
`-P`. The port can be define with the environment variable `OKOM_PORT`
or with the option `-p`. 

To copy the files and templates to `okom_tutorial` you can either run

```
okom -P okom_tutorial copy-files
```

or

```
export OKOM_PATH=okom_tutorial
okom copy-files
```


This copies the HTML templates and the CSS and JavaScript files used
by okom. You can edit this files to modify the appearance or behavior
of the program. 

The main website is a login page for doctors. To add a new doctor
account you can either run

```
okom -P okom_tutorial add-doctor
```

or 

```
export OKOM_PATH=okom_tutorial
okom add-doctor
```

Then it is necessary to add a folder called exercises and add
sub-folders for each exercises that one wants to make available in the
platform. For this we can use the [okom exercises
library](https://gitlab.com/oquijano/okom_library) git repository. 

```
cd okom_tutorial
git clone https://gitlab.com/oquijano/okom_library.git exercises
```

Finally, while being inside the `okom_tutorial` folder we can start
the platform on port 8080 by running

```
okom -P . -p 8080 start
```

or

```
export OKOM_PATH=.
export OKOM_PORT=8080
okom start
```

After this you can visit
[http://localhost:8080](http://localhost:8080) with the browser of
your preference and you should see the okom login page.


<!--  LocalWords:  Okom okom
-->
	
